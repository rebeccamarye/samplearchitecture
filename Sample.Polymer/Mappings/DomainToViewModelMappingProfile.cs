﻿using AutoMapper;
using Sample.Models;
using Sample.Polymer.Models;

namespace Sample.Polymer.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        public DomainToViewModelMappingProfile()
        {
            //CreateMap<From, To>();
            CreateMap<Widget, WidgetViewModel>();
        }
        
   }
}