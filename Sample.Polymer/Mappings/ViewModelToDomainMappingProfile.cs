﻿using AutoMapper;
using Sample.Models;
using Sample.Polymer.Models;

namespace Sample.Polymer.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }


        public ViewModelToDomainMappingProfile()
        {
            //CreateMap<From, To>();
            CreateMap<WidgetViewModel, Widget>();
        }

    }
}
