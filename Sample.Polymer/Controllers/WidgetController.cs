﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

using AutoMapper;
using Sample.Models;
using Sample.Services;
using Sample.Polymer.Models;

namespace Sample.Polymer.Controllers
{
    public class WidgetController : Controller
    {
        private readonly IWidgetService widgetService;

        public WidgetController(IWidgetService widgetService)
        {
            this.widgetService = widgetService;
        }

        // GET: Widget
        public ActionResult Index()
        {
            IEnumerable<Widget> widgets;
            IEnumerable<WidgetViewModel> widgetViewModels;

            widgets = this.widgetService.GetWidgets();
            widgetViewModels = Mapper.Map<IEnumerable<Widget>, IEnumerable<WidgetViewModel>>(widgets);

            return View(widgetViewModels);
        }

        // GET: Widget/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var widget = this.widgetService.GetWidget(id.Value);
            if (widget == null)
            {
                return HttpNotFound();
            }
            var widgetViewModel = Mapper.Map<WidgetViewModel>(widget);
            return View("Details", widgetViewModel);
        }

        // GET: Widget/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Widget/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WidgetViewModel widgetViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(widgetViewModel);
            }

            var widget = Mapper.Map<Widget>(widgetViewModel);
            this.widgetService.CreateWidget(widget);
            this.widgetService.SaveWidget();

            return RedirectToAction("Details", new { id = widget.Id });
        }

        // GET: Widget/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var widget = this.widgetService.GetWidget(id.Value);
            if (widget == null)
            {
                return HttpNotFound();
            }
            var widgetViewModel = Mapper.Map<WidgetViewModel>(widget);
            return View("Edit", widgetViewModel);
        }

        // POST: Widget/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(WidgetViewModel widgetViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(widgetViewModel);
            }

            var widget = this.widgetService.GetWidget(widgetViewModel.Id);
            widget = Mapper.Map(widgetViewModel, widget);
            this.widgetService.UpdateWidget(widget);
            this.widgetService.SaveWidget();

            return View("Details", widgetViewModel);
        }

        // GET: Widget/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var widget = this.widgetService.GetWidget(id.Value);
            if (widget == null)
            {
                return HttpNotFound();
            }
            var widgetViewModel = Mapper.Map<WidgetViewModel>(widget);
            return View(widgetViewModel);
        }

        // POST: Widget/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var widget = this.widgetService.GetWidget(id);
            this.widgetService.DeleteWidget(widget);
            this.widgetService.SaveWidget();
            
            return RedirectToAction("Index");
        }

    }
}
