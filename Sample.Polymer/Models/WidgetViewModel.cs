﻿using System.ComponentModel.DataAnnotations;

namespace Sample.Polymer.Models
{
    public class WidgetViewModel
    {
        [Display(Name = "Identifier")]
        public int Id { get; set; }
        [Required]
        public string SomeValue { get; set; }
    }
}
