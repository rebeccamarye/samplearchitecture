﻿using Sample.Data.Infrastructure;
using Sample.Models;

namespace Sample.Data.Repositories
{
    public class WidgetRepository : RepositoryBase<Widget>, IWidgetRepository
    {
        public WidgetRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }

    public interface IWidgetRepository : IRepository<Widget>
    {

    }
}
