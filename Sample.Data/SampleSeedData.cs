﻿using System.Data.Entity;

namespace Sample.Data
{
    public class SampleSeedData : DropCreateDatabaseIfModelChanges<SampleEntities>
    {
        protected override void Seed(SampleEntities context)
        {
            base.Seed(context);
        }
    }
}
