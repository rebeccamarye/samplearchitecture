﻿using System;

namespace Sample.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        SampleEntities Init();
    }
}