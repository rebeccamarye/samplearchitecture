﻿namespace Sample.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}