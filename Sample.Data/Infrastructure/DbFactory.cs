﻿namespace Sample.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private SampleEntities dbContext;

        public SampleEntities Init()
        {
            return dbContext ?? (dbContext = new SampleEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}