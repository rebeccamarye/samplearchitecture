﻿using System.Data.Entity;
using Sample.Data.Configuration;
using Sample.Models;

namespace Sample.Data
{
    public class SampleEntities: DbContext
    {
        public SampleEntities() : base("SampleEntities")
        {
            Database.SetInitializer(new SampleSeedData());
        }

        public DbSet<Widget> Widget { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new WidgetConfiguration());
        }

        public System.Data.Entity.DbSet<Sample.Models.Widget> Widgets { get; set; }
    }
}
