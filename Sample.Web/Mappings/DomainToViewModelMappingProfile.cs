﻿using AutoMapper;
using Sample.Models;

namespace Sample.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        public DomainToViewModelMappingProfile()
        {
            //CreateMap<From, To>();
        }
        
   }
}