﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calc.Models
{
    public class CalculationVariable
    {
        public Calculation Calculation { get; set; }
        public int Id { get; set; }
        public string VariableName { get; set; }
        public int Ordinal { get; set; }
        public string Value { get; set; }

    }
}
