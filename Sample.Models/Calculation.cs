﻿namespace Calc.Models
{
    public class Calculation
    {
        public Strategy Strategy { get; set; }
        public int Id { get; set; }
        public string CalculationName { get; set; }
        public int Ordinal { get; set; }
    }
}
