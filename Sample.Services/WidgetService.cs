﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Sample.Data.Infrastructure;
using Sample.Data.Repositories;
using Sample.Models;

namespace Sample.Services
{
    public class WidgetService : IWidgetService
    {
        private readonly IWidgetRepository sampleRepository;
        private readonly IUnitOfWork unitOfWork;

        public WidgetService(IWidgetRepository sampleRepository, IUnitOfWork unitOfWork)
        {
            this.sampleRepository = sampleRepository;
            this.unitOfWork = unitOfWork;
        }

        public void CreateWidget(Widget sample)
        {
            sampleRepository.Add(sample);
        }

        public void DeleteWidget(Expression<Func<Widget, bool>> where)
        {
            sampleRepository.Delete(where);
        }

        public void DeleteWidget(Widget sample)
        {
            sampleRepository.Delete(sample);
        }

        public IQueryable<Widget> GetMany(Expression<Func<Widget, bool>> where)
        {
            return sampleRepository.GetMany(where);
        }

        public Widget GetWidget(int id)
        {
            return sampleRepository.GetById(id);
        }

        public IQueryable<Widget> GetWidgets()
        {
            return sampleRepository.GetAll();
        }

        public void SaveWidget()
        {
            unitOfWork.Commit();
        }

        public void UpdateWidget(Widget sample)
        {
            sampleRepository.Update(sample);
        }
    }

    public interface IWidgetService
    {
        IQueryable<Widget> GetWidgets();

        IQueryable<Widget> GetMany(Expression<Func<Widget, bool>> where);

        Widget GetWidget(int id);

        void CreateWidget(Widget sample);

        void UpdateWidget(Widget sample);

        void DeleteWidget(Widget sample);

        void DeleteWidget(Expression<Func<Widget, bool>> where);

        void SaveWidget();
    }
}
